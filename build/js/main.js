let delay = 3000;
let i = 0;

$(document).ready(function () {
    $('.crystal__figure').click(function () {
        $(".crystal__figure").toggleClass('active');
    });

    setTimeout(function run() {
        $(".content").text(i > 2 ? 'Go!' : i + 1);
        if (i === 3) {
            i = 0;
        } else {
            i++;
        }
        if (i === 0) {
            delay = 3000;
        } else {
            delay = 1000
        }
        setTimeout(run, delay);
    }, delay);
});